<?php

require_once 'db.php';

//if (isset($argv[1])) {
//    $table = $argv[1];
//} else {
//    $table = "ebay_unixpluscom";
//}
//if (isset($argv[2])) {
//    $file = $argv[2];
//} else {
//    $file = "unixpluscomchannel.csv";
//}
$tables = array(
    "ebay_unixpluscom" => "unixpluscomchannel.csv",
    "ebay_unixsurplusnet" => "unixsurplusnetchannel.csv",
    "ebay_unixsurpluscom" => "unixsurpluscomchannel.csv",
    "ebay_itrecyclenow" => "itrecyclenowchannel.csv"
);
runsql("truncate ebay");
print "eBay Database Truncated.\n"; 
foreach ($tables as $table => $file) {
    runsql("truncate $table");
    $loadsql = '
    LOAD DATA LOCAL INFILE "' . $file . '"
        INTO TABLE ' . $table . '
        FIELDS
                TERMINATED by \',\'
                OPTIONALLY ENCLOSED BY \'"\' 
                ESCAPED BY \'\'
        LINES
                TERMINATED BY \'\n\' ';
    $load = runsql($loadsql);
    $ebays = runsql("select par, ad from $table where ad <> ''");
    foreach ($ebays as $ebay) {
        runsql("insert into ebay (par, $table) values ('$ebay[par]', '$ebay[ad]') on duplicate key update $table = '$ebay[ad]'");
    }
    print $table.": ".count($ebays)." skus \n";
    //print "insert into ebay (par, $table) values ('$ebay[par]', '$ebay[ad]') on duplicate key update $table = '$ebay[ad]'";
}


        
