<?php

require_once 'db.php';
function csv($data, $filename = 'brokerbinexport') {
    $bbfields = array(
    "par" => "part number",
    "man" => "manufacturer",
    "con" => "condition",
    "pri" => "price",
    "qty" => "qty",
    "des" => "description"
);
// create a file pointer connected to the output stream
    $file = fopen('php://output', 'w');
// output each row of the data
    fputcsv($file, $bbfields);
    foreach ($data as $row) {
        fputcsv($file, $row);
    }
    exit();
}
function cpulist($cpus, $cpudes, $sc = 1) {
    $newlist = array();
    foreach ($cpus as $cpu) {
        $par = $cpu['par'];
        if ($cpu['qty'] > 100 ) {$cpu['qty'] =100;}
        if (substr($par, -2, 1) == "G" || substr($par, -2, 1) == "g") {
            $parnog = substr($par, 0, strlen($par) - 2);
            $des = runsql("select $cpudes as des, step_code as psc from intel_ark where proc_name = '$parnog'");
            if ($des){
                $grade = substr($par, -1);
                $cpu['des'] = $des[0]['des'] . " Grade $grade";
                $cpu['psc'] = $des[0]['psc'] . "G" . $grade;
            }
        }
        $newlist[$par] = array(
            'par' => $cpu['par'],
            'man' => $cpu['man'],
            'con' => $cpu['con'],
            'pri' => $cpu['pri'],
            'qty' => $cpu['qty'],
            'des' => $cpu['des']
        );
        if ($sc) {
            $psc = $cpu['psc'];
            $newlist[$psc] = $newlist[$par];
            $newlist[$psc]['par'] = $psc;
        }
    }
    return $newlist;
}
function invlist ($inv, $price = 1){
    $newlist =array();
    foreach ($inv as $item){
        $par = $item['par'];
        if ($item['qty'] > 100 ) {$item['qty'] = 100;}
        if (!$price){$item['pri'] = 0;}
        $newlist[$par] = array(
            'par' => $item['par'],
            'man' => $item['man'],
            'con' => $item['con'],
            'pri' => $item['pri'],
            'qty' => $item['qty'],
            'des' => $item['des']
        );
    }
    return $newlist;
}
$cpudes = "concat('Intel Xeon ', proc_name, ' ', step_code, ' ', speed_base, 'GHz ', num_cores,'C ', tdp, 'W ',cast(cache as signed),'MB ', code_name,' ', socket)";
$cpus = runsql("select par, man, con, pri, qty, $cpudes as des, step_code as psc from inventory left join intel_ark on(par=proc_name) where cat = 'CPU' and qty > 0 order by par");

$servers = runsql("select par, man, con, pri, qty, des from inventory where cat = 'Server' and qty > 0 and (mid(par,2,1)<>'U') order by par");
$uservers = runsql("select mid(par,locate('-',par)+1) as par, man, con, pri, qty, des from inventory where cat = 'Server' and qty > 0 and (mid(par,2,1)='U') order by par");
$inventory = runsql("select par, man, con, pri, qty, des from inventory  where qty > 0 and cat <> 'CPU' and cat <> 'Server' and cat <> 'Memory'");
$memory = runsql("select par, man, con, pri, qty, des from inventory  where cat = 'Memory'");
$inv = array_merge(invlist($memory, 0), invlist($inventory),cpulist($cpus, $cpudes, 1),invlist($servers),invlist($uservers));
csv($inv); 




