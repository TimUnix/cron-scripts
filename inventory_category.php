<?php

require_once 'db.php';

$rn = "\n";

$y = date("Y");
$ly = $y - 1;
$m = date("m");
$d = date("d");
for ($i = 0; $i < 12; $i++) {
    $dates[$i]['y'] = $ly;
    $dates[$i]['m'] = (($m + $i) % 12) + 1;
    $dates[$i]['d'] = 1;
    if ($dates[$i]['m'] <= $m) {
        $dates[$i]['y'] = $y;
    }
}

$skip = 0;
$start = microtime(true);
if (!$skip) {
    runsql("truncate inventory_monthly");
    foreach ($dates as $dt) {
        echo "Monthly {$dt['m']} {$dt['y']} Inserted $rn";
        runsql("insert into inventory_monthly select * from inventory_history where y = {$dt['y']} and m = {$dt['m']} and d = {$dt['d']}");
        echo "insert into inventory_monthly select * from inventory_history where y = {$dt['y']} and m = {$dt['m']} and d = {$dt['d']} $rn";
    }
    $items = runsql("SELECT par, cat FROM `inventory_history` WHERE y={$dates[11]['y']} and m = {$dates[11]['m']} and d = {$dates[11]['d']} and cat ='NVME'");

    foreach ($items as $item) {
        runsql("update `inventory_monthly` set cat = '{$item['cat']}' where par = '{$item['par']}'");
        //echo "{$item['par']} Updated $rn";
    }
}

runsql("truncate inventory_category");
foreach ($dates as $dt) {
    $sql = "insert into `inventory_category` SELECT ih_id, y, {$dt['m']}, {$dt['d']}, cat, count(*) as ite, sum(qty) as qty, sum(pri*qty) as val FROM `inventory_monthly` where y = {$dt['y']} and m = {$dt['m']} and d = {$dt['d']} group by cat";
    $cats = runsql($sql);
    echo "Category {$dt['m']} {$dt['y']} Inserted $rn";
}

echo "Total Time: " . ((microtime(true) - $start) / 60) . "min" . $rn;
