<?php

require_once 'db.php';
$table = "inventory_history";
$rn = "\n";

//runsql("TRUNCATE $table");
$file_names = scandir("/home/ironhyde/backup/");
array_shift($file_names);
array_shift($file_names);
$startstart = microtime(true);
$lastmonth_file_names = array_slice($file_names, -10,10, true);
foreach ($file_names as $file_name) {
    $start = microtime(true);
    $info['y'] = substr($file_name, 12, 4);
    $info['m'] = substr($file_name, 17, 2);
    $info['d'] = substr($file_name, 20, 2);
    $file = "/home/ironhyde/backup/" . $file_name;
    $exist = 0;
    $exist = runsql("select * from $table where y = {$info['y']} and m = {$info['m']} and d= {$info['d']} limit 1");
    echo $file_name." - ";
    if (!$exist) {
        $handle = fopen($file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $data = explode(',', $line);
                $sql = "insert into $table (par, bar, man, con, pri, qty, des, cat, bin, y, m, d) values"
                        . "($data[0],$data[1],$data[2],$data[3],$data[4],$data[5],$data[6],$data[7],$data[8], {$info['y']}, {$info['m']}, {$info['d']})";
                runsql($sql);
            }
            fclose($handle);
        } else {
            echo "unable to open $file" . $rn;
        }
        $end = microtime(true);
        echo "Time: " . ($end - $start).$rn;
    }else{
        echo "EXIST".$rn;
    }
}
echo "Total Time: ".((microtime(true)-$startstart)/60)."min".$rn;
runsql("delete from $table where par =''");
