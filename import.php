<?php

require_once 'db.php';

if (isset($argv[1])) {
    $table = $argv[1];
} else {
    $table = "inventory";
}
if (isset($argv[2])) {
    $file = $argv[2];
} else {
    $file = "all.csv";
}

function updatereport($data, $inventory = "inv") {
    $y = date("y");
    $d = date("z");
    $h = date("H");
    foreach ($data as $cat) {
        //debug($cat);
        $rid = $y . $d . $h . $inventory . $cat['cat'];
        $cat['val'] = (int) $cat['val'];
        $sql = "insert into inventory_report (rid,y,d,h, inv, cat, ite, qty, val) values('$rid',$y,$d,$h, '$inventory','{$cat['cat']}',{$cat['tot']},{$cat['qty']},{$cat['val']}) on duplicate key update ite = {$cat['tot']}, qty = {$cat['qty']}, val = {$cat['val']}";
        runsql($sql);
    }
}

$trunc = mysqli_query($cons, "truncate $table");
$loadsql = '
    LOAD DATA LOCAL INFILE "' . $file . '"
        INTO TABLE ' . $table . '
        FIELDS
                TERMINATED by \',\'
                OPTIONALLY ENCLOSED BY \'"\'
                ESCAPED BY \'\'
        LINES
                TERMINATED BY \'\n\'
';
$load = runsql($loadsql);
runsql("delete from $table where cat = 'category'");
//runsql("update $table set des = replace(des,'\"','in') where des like '%\"%'");
//runsql("update $table set des = replace(des,'\\','in') where des like '%\\%'");
$count = runsql("select count(*) from $table", 1);
echo "$count records have been added to the table $table\n";
$bycats = runsql("select cat, count(*) as qty, sum(qty) as tot, sum(pri*qty) as val  from $table where qty >0 and arc='False' and bin <> '' and cat <> 'System' and cat <> 'Server' and cat <> 'Lots' and cat <> 'Builds' group by cat");
print updatereport($bycats, substr($table, 0, 3));
