<?php

require_once 'db.php';

if (isset($argv[1])) {
    $table = $argv[1];
} else {
    $table = "images";
}
if (isset($argv[2])) {
    $file = $argv[2];
} else {
    $file = "images.csv";
}

$trunc = mysqli_query($cons, "truncate $table");
$loadsql = '
    LOAD DATA LOCAL INFILE "' . $file . '"
        INTO TABLE ' . $table . '
        FIELDS
                TERMINATED by \',\'
                OPTIONALLY ENCLOSED BY \'"\'
                ESCAPED BY \'\'
        LINES
                TERMINATED BY \'\n\'
';
$load = runsql($loadsql);
$noimg = runsql("SELECT inventory.par FROM `inventory` left join images using(par) where isnull(img) and arc = 'False'");

foreach($noimg as $par){
    $id = $par['par'];
    runsql("INSERT INTO `images` (`par`, `img`, `main`, `sort_order`) VALUES ('$id', '/i/noimage.png', 'True', '0')");
}
        
